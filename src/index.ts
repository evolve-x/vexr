import SXEVR from './Structures/SVEXR';
import config from '../config.json';

const svexr = new SXEVR(config);

svexr.init();
