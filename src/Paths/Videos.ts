/**
 * @license
 *
 * SVEXR Is a Evolve-X router for files/shortened links.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import Path from '../Structures/Path';
import SVEXR from '../Structures/SVEXR';
import Base from '../Structures/Base';
import { Response } from 'express';
import superagent from 'superagent';

class Video extends Path {
    constructor(SVEXR: SVEXR, base: Base) {
        super(SVEXR, base);
        this.label = 'Videos';
        this.path = ['/v/:ID', '/videos/:ID'];
        this.type = 'get';
    }

    /**
     * @desc PONG! Just a simple response, no auth needed
     */
    async execute(req: any, res: any): Promise<Response> {
        if (!req.params || !req.params.ID) {
            return res.status(this.codes.badReq).send('Input a ID into the URL next time.');
        }
        const file = await superagent.get(`${this.base.options.url}/videos/${req.params.ID}`);
        if (!file || file.status === this.codes.notFound || file.status === this.codes.badReq) {
            return res.status(this.codes.notFound).send('File not found or bad request');
        }
        res.set( {
            'Content-Type': file.type,
            'Content-Length': file.header["content-length"],
        } );
        return res.send(file.body);
    }
}

export default Video;
