/**
 * @license
 *
 * SVEXR Is a Evolve-X router for files/shortened links.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import Path from '../Structures/Path';
import SVEXR from '../Structures/SVEXR';
import Base from '../Structures/Base';
import { Response } from 'express';

class Shorten extends Path {
    constructor(SVEXR: SVEXR, base: Base) {
        super(SVEXR, base);
        this.label = 'Shorten';
        this.path = ['/short/:ID'];
        this.type = 'get';
    }

    /**
     * @desc PONG! Just a simple response, no auth needed
     */
    async execute(req: any, res: any): Promise<Response | void> {
        if (!req.params || !req.params.ID) {
            return res.status(this.codes.badReq).send('Input a ID into the URL next time.');
        }
        let r;
        try {
            r = await this.base.superagent.get(`${this.base.options.url}/short/${req.params.ID}?u=t`);
        } catch (e) {
            return res.send(e);
        }
        if (!r || r.status !== this.codes.ok) {
            return res.status(this.codes.internalErr).send('Invalid Upstream Response');
        }
        res.redirect(`${r.text}`);
    }
}

export default Shorten;
