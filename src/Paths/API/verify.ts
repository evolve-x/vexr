/**
 * @license
 *
 * SVEXR Is a Evolve-X router for files/shortened links.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import Path from '../../Structures/Path';
import SVEXR from '../../Structures/SVEXR';
import Base from '../../Structures/Base';
import { Response } from 'express';

class Verify extends Path {
    constructor(SVEXR: SVEXR, base: Base) {
        super(SVEXR, base);
        this.label = '[API] Verify';
        this.path = '/api/verify';
        this.type = 'get';
    }

    /**
     * @desc Verify Evolve-X instance
     */
    async execute(req: any, res: any): Promise<Response> {
        if (!req.body || (!req.body.url || !req.body.owner) ) {
            return res.status(this.codes.badReq).send('[FAIL] Verification failed.');
        }
        if (req.body.url !== this.base.options.url || (req.body.owner !== this.base.options.owner && !this.base.options.allowOthers) ) {
            return res.status(this.codes.badReq).send('[FAIL] Verification failed.');
        }
        let end: any = { url: this.base.options.url, "authentic-vexr": true };
        end = JSON.stringify(end);
        return res.status(this.codes.ok).send(end);
    }
}

export default Verify;
