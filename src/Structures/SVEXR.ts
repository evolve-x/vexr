/**
 * @license
 *
 * SVEXR Is a Evolve-X router for files/shortened links.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

// Taken from https://gitlab.com/evolve-x/evolve-x/tree/master/src/Structures/Evolve.ts
// and modified to fit the need of SVEXR

/**
 * @author VoidNulll
 * Contact me at https://gitlab.com/VoidNulll
 * @version 0.8.0
 */

/* eslint require-atomic-updates: "off" */

import Base from './Base';
import { Options } from './SVEXR-Config';
import * as paths from '../Paths';
import Path from './Path';
import { join } from 'path';
import { Request, Response } from 'express';

/**
 * @class Evolve
 *
 * @classdesc Base client for Evolve-X, handles banning IPs in memory and security for the frontend
 *
 * @author Null#0515
 */
class Evolve {
    private _options: Options;

    public paths: Map<string, object>;

    private base: Base;

    /**
     * @param {Object} options The options to pass to the base of the client
     *
     * @prop {Object} _options The options
     * @prop {Map} paths The Evolve-X paths
     * @prop {Map} ips The ips requesting evolve-x
     * @prop {String[]} ipBans The IPs temporarily banned
     */
    constructor(options: Options) {
        this._options = options;
        this.paths = new Map();
        this.handleHeaders = this.handleHeaders.bind(this);
        this.base = new Base(this, this._options);
    }



    /**
     * @desc Initialize a path
     *
     * @param {Object<Path>} path The path to initialize
     * @param {Object} base The base of evolve-x
     * @private
     */
    _initPath(path: Path, base: Base): void {
        // Handle if the path is a bad path
        if (!path.label || !path.path) {
            throw Error(`[ERROR] Path ${path.path || path.label} label and or path not found!`);
        }
        if (!path.execute) {
            throw Error(`[ERROR] Path ${path.label} does not have an execute method!`);
        }
        // Set the path, then initiate the path on the web server. I will probably set up a better method later
        this.paths.set(path.label, path);

        // Init the path with the web app
        if (path.type === 'post') {
            base.web.post(path.path, (req, res) => path._execute(req, res) );
        } else if (path.type === 'delete') {
            base.web.delete(path.path, (req, res) => path._execute(req, res) );
        } else if (path.type === 'patch') {
            base.web.patch(path.path, (req, res) => path._execute(req, res) );
        } else {
            base.web.get(path.path, (req, res) => path._execute(req, res) );
        }
    }

    /**
     * @desc Handles setting security headers for the request for the app to respond with
     * @param req ExpressJS Request
     * @param res ExpressJS Response
     * @param next {function} ExpressJS middleware next function
     * @returns {void}
     */
    handleHeaders(req: Request, res: Response, next: any): void {
        if (!this.base || !this.base.options.certOptions || !this.base.options.certOptions.cert || !this.base.options.certOptions.key) {
            res.set( {
                'Content-Security-Policy': 'default-src \'self\'; script-src \'self\' \'unsafe-inline\' cdnjs.cloudflare.com polyfill.io unpkg.com; style-src \'self\' \'unsafe-inline\' fonts.googleapis.com cdnjs.cloudflare.com; img-src \'self\' https://*; frame-src \'none\'; font-src \'self\' fonts.gstatic.com cdnjs.cloudflare.com',
                'X-Frame-Options': 'DENY',
                'Referrer-Policy': 'no-referrer, origin-when-cross-origin',
                'X-XSS-Protection': '1; mode=block',
                'X-Content-Type-Options': 'nosniff',
                'Access-Control-Allow-Origin': '*',
            } );
        } else {
            res.set( {
                'Content-Security-Policy': 'default-src \'self\'; script-src \'self\' \'unsafe-inline\' cdnjs.cloudflare.com polyfill.io unpkg.com; style-src \'self\' \'unsafe-inline\' fonts.googleapis.com cdnjs.cloudflare.com; img-src \'self\' https://*; frame-src \'none\'; font-src \'self\' fonts.gstatic.com cdnjs.cloudflare.com',
                'X-Frame-Options': 'DENY',
                'Referrer-Policy': 'no-referrer, origin-when-cross-origin',
                'X-XSS-Protection': '1; mode=block',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
                'Access-Control-Allow-Origin': '*',
            } );
        }
        next();
    }

    /**
     * @desc Initialize the base and evolve-x
     *
     * @returns {Promise<void>}
     */
    async init(): Promise<void> {
        // Init the base, remove options
        const { base } = this;
        delete this._options;
        base.web.use('*', this.handleHeaders);
        // Initiate paths
        let pathNums = 0;
        for (const path in paths) {
            const mName: string = path;
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            const Ok = paths[path];
            const apath: Path = new Ok(this, base);
            if (apath.enabled) { // If the path should be loaded
                console.log(`[SYSTEM INIT PATH] - Initializing Path ${apath.label}`);
                // Init the path
                this._initPath(apath, base);
                // Tell the user the path was initialized and add the number of paths loaded by 1
                console.log(`[SYSTEM INIT PATH] - Initialized path ${apath.label} (${mName}) with type ${apath.type}!`);
                pathNums++;
            }
        }
        console.log(`[SYSTEM INIT] Initialized ${pathNums} paths`);
        // Initiate the base of the project
        await base.init();

        this.base.Logger.log('SYSTEM INFO', 'SVEXR has been initialized!', {}, 'online', 'Evolve-X is online');
        if (process.env.NODE_ENV === 'test') {
            process.exit();
        }
    }
}

export default Evolve;
