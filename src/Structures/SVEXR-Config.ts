/**
 * @license
 *
 * SVEXR Is a Evolve-X router for files/shortened links.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @author VoidNulll
 * @version 0.8.0
 */

// Taken from https://gitlab.com/evolve-x/evolve-x/tree/master/src/Structures/Evolve-Config.ts
// and modified to fit the need of SVEXR

// I took inspiration from https://github.com/Khaazz/AxonCore/blob/dev-2.0/src/AxonOptions.js for this
// Hope you do not mind, KhaaZ.

interface CertOptions {
    key?: string | any;
    cert?: string | any;
    requestCert?: boolean;
    ca?: string[] | any[];
}

export interface DiscordHook {
    name?: string;
    avatar_url?: string;
}

export interface Options {
    port?: number;
    url: string;
    owner: string;
    allowOthers: boolean;
    trustProxies?: boolean;
    certOptions?: CertOptions;
    discordURL?: string;
    enableDiscordLogging?: boolean;
    discordHook?: DiscordHook;
}

export interface ActualOptions {
    port: number;
    url: string;
    owner: string;
    allowOthers?: boolean;
    trustProxies: boolean;
    certOptions?: CertOptions;
    discordURL?: string;
    enableDiscordLogging?: boolean;
    discordHook?: DiscordHook;
}

const optionsBase: ActualOptions = {
    port: 8888,
    trustProxies: false,
    url: 'localhost:8888',
    owner: '',
    allowOthers: false,
};

/**
 * @class EvolveConfig
 *
 * @classdesc Class for generating & validating the Evolve-x config
 *
 * @author Null
 */
class SVEXRConfig implements ActualOptions {
    /**
     * @param {Object<Options>} config=optionsBase The configuration for the app from the user.
     * @implements ActualOptions
     *
     * @property {Number} port  The port the application will use
     * @property {String} url The URL the application will use
     */
    public port: number;

    public url: string;

    public owner: string;

    public allowOthers?: boolean;

    public trustProxies: boolean;

    public certOptions?: CertOptions;

    public enableDiscordLogging?: boolean;

    public discordURL?: string;

    public discordHook?: DiscordHook;

    constructor(config: Options = optionsBase) {
        this.port = config.port || optionsBase.port;
        this.url = config.url;
        this.owner = config.owner;
        this.trustProxies = config.trustProxies || optionsBase.trustProxies;
        this.discordURL = config.discordURL;
        this.enableDiscordLogging = config.enableDiscordLogging || false;
        this.certOptions = {
            key: (config && config.certOptions && config.certOptions.key),
            cert: (config && config.certOptions && config.certOptions.cert),
            ca: (config && config.certOptions && config.certOptions.ca),
            requestCert: (config && config.certOptions && Boolean(config.certOptions.requestCert) ),
        };
        this.discordHook = config.discordHook;
        this.verify();
    }

    /**
     * @private
     *
     * @desc Verifies the majority of the Evolve configuration.
     *
     * @returns {Object<ActualOptions>} The finale options.
     */
    private verify(): ActualOptions {
        const maxPort = 65535;
        if (this.port > maxPort || isNaN(Number(this.port) ) ) {
            this.port = 8888;
        }
        if (!this.url) {
            console.log('[CONFIG - FAIL] - URL is a REQUIRED value');
        }
        return this;
    }
}

export default SVEXRConfig;
