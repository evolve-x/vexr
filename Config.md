# Evolve-X config

### Keys

Port - The port to listen to on your machine

- If you are on linux, I recommend to not go below port 1024 and not to run this as root
- Bad things could happen but shouldn't, but its safe to go with recommended practices.

url - THe Evolve-X instance URL

trustProxies - Whether or not to trust proxies (useful for if you are running behind a reverse proxy like nginx)

certOptions - Object containing options for https.

certOptions.cert - COMPLETE path to cert (Not relative).

certOptions.ca - Only useful to people who use self signed certs.

certOptions.key - Exact path to certificate key.

certOptions.requestCert - This is necessary only if using client certificate authentication.

discordURL - String of a valid discord webhook URL

enableDiscordLogging - Boolean of if discord logging should be enabled

discordHook - Object containing name &/or avatar to overwrite when sending the webhook

discordHook.avatar_url - Image URL to overwrite the webhooks avatar

discordHook.name - New name to overwrite when sending the webhook

### Discord webhooks

Learn about webhooks at https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks

### Example config

```json
{
    "url": "localhost:8888",
    "port": 1337,
    "enableDiscordLogging": true,
    "discordURL": "/webhook/url",
    "DiscordHook": { "name": "Evolve-X" }
}
```

### Default config

```json
{
    "url": "localhost",
    "port": 8888,
    "mongoUrl": "mongodb://localhost/evolve-x",
    "apiOnly": false,
    "signups": true,
    "enableDiscorddLogging": false
}
```
